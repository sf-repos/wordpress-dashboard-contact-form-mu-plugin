# dashboard contact form

A contact form for the admin dashboard

1)This is designed to be a MU plugin, so just drag it in the mu-plugins folder

2)change the $to mailer variable to the email you want it to send to (line 66)

Dashboard with this plugin active:

![picture](https://bitbucket.org/sf-repos/wordpress-dashboard-contact-form-mu-plugin/raw/master/screenshots/dashboard.png)

Validation:

![picture](https://bitbucket.org/sf-repos/wordpress-dashboard-contact-form-mu-plugin/raw/master/screenshots/error.png)

The contact form:

![picture](https://bitbucket.org/sf-repos/wordpress-dashboard-contact-form-mu-plugin/raw/master/screenshots/form.png)