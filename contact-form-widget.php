<?php
/*
Plugin Name: Contact Form Widget
Plugin URI: https://bitbucket.org/sf-repos/wordpress-dashboard-contact-form-mu-plugin/
Description: A contact form for the dashboard
Version: 1.0
Author: Sam Fullalove
Author URI: http://sam.fullalove.co/
Text Domain: contact-form-widget
License: GPLv2
*/


add_action('wp_dashboard_setup', 'cfw_add_dashboard_widget');


/**
 * call function to create our dashboard widget
 */
function cfw_add_dashboard_widget()
{

  wp_add_dashboard_widget(
    'cfw_dashboard_widget',
    'Need help?',
    'cfw_create_dashboard_widget'
  );
}

/**
 * function to display our dashboard widget content
 */
function cfw_create_dashboard_widget()
{

  //response generation function

  global $response;

  /**
   * function to generate success / error response
   */
  function my_contact_form_generate_response($type, $message)
  {

    global $response;

    if ($type == "success") $response = "<p class='success'>{$message}</p>";
    else $response = "<p class='error'>{$message}</p>";
  }

  //response messages
  $not_human       = "Human verification incorrect.";
  $missing_content = "Please supply all information.";
  $email_invalid   = "Email Address Invalid.";
  $message_unsent  = "Message was not sent. Try Again.";
  $message_sent    = "Thanks! Your message has been sent.";

  //user posted variables
  $name = $_POST['message_name'];
  $email = $_POST['message_email'];
  $message = $_POST['message_text'];
  $human = $_POST['message_human'];

  //php mailer variables
  $to = 'xxxx@xxxxxxxxxxxxxxxxxx.com';
  $subject = "Someone sent a message from " . get_bloginfo('name');
  $headers = 'From: ' . $name . " <" . $email . ">\r\n" . //without this $name it sends from wordpress
    'Reply-To: ' . $email . "\r\n";

  if (!$human == 0) {
    if ($human != 2) my_contact_form_generate_response("error", $not_human); //not human!
    else {

      //validate email
      if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        my_contact_form_generate_response("error", $email_invalid);
      else //email is valid
      {
        //validate presence of name and message
        if (empty($name) || empty($message)) {
          my_contact_form_generate_response("error", $missing_content);
        } else //ready to go!
        {
          $sent = wp_mail($to, $subject, strip_tags($message), $headers);
          if ($sent) my_contact_form_generate_response("success", $message_sent); //message sent!
          else my_contact_form_generate_response("error", $message_unsent); //message wasn't sent
        }
      }
    }
  } else if ($_POST['submitted']) my_contact_form_generate_response("error", $missing_content);

?>
  <style type="text/css">
    .error {
      padding: 5px 9px;
      border: 1px solid red;
      color: red;
      border-radius: 3px;
    }

    .success {
      padding: 5px 9px;
      border: 1px solid green;
      color: green;
      border-radius: 3px;
    }

    form span {
      color: red;
    }
  </style>

  <div id="respond">
    <?php echo $response; //display success/ error message 
    ?>
    <form action="" method="post">
      <p><label for="name">Name: <span>*</span> <br><input type="text" name="message_name" value="<?php echo esc_attr($_POST['message_name']); ?>"></label></p>
      <p><label for="message_email">Email: <span>*</span> <br><input type="text" name="message_email" value="<?php echo esc_attr($_POST['message_email']); ?>"></label></p>
      <p><label for="message_text">Message: <span>*</span> <br><textarea type="text" name="message_text"><?php echo esc_textarea($_POST['message_text']); ?></textarea></label></p>
      <p><label for="message_human">Human Verification: <span>*</span> <br><input type="text" style="width: 60px;" name="message_human"> + 3 = 5</label></p>
      <input type="hidden" name="submitted" value="1">
      <p><input type="submit"></p>
    </form>
  </div>
<?php
}
?>